import pandas as pd
import Graph as gr

data = pd.read_csv('Dataset_Entrevista_Walmart.csv',encoding='utf-8-sig',engine='python',converters = {'Upc':lambda x:str.strip(x)})

gra =gr.Grafica(data)

gra.venta_ticket()
gra.barra_ven_dia()
gra.barra_sun_dep()
gra.barra_mon_dep()
gra.barra_tus_dep()
gra.barra_wed_dep()
gra.barra_thu_dep()
gra.barra_fri_dep()
gra.barra_sat_dep()

