import pandas as pd
import matplotlib.pyplot as plt

class Grafica():
	def __init__(self, data):
		self.data = data 
	#print(data.info())

	#Numero de ventas por ticket
	def venta_ticket(self):
		ventas_t = self.data[['VisitNumber','ScanCount']]
		ventas_t= ventas_t.groupby(['VisitNumber']).sum()
		id_tick = list(ventas_t.index.get_level_values('VisitNumber'))
		count = list(ventas_t['ScanCount'])
		#ventas_t = pd.DataFrame({'VisitNumber':id_tick,'ScanCount':count})
		fig = plt.figure(u'Historial de ticket') 
		plt.plot(id_tick,count)
		return plt.show()



	#numero de ventas por dia 
	def barra_ven_dia(self):
		ventas_d = self.data[['Weekday','ScanCount']]
		ventas_d= ventas_d.groupby(['Weekday']).sum()
		week = list(ventas_d.index.get_level_values('Weekday'))
		count = list(ventas_d['ScanCount'])
		#ventas_t = pd.DataFrame({'Weekday':id_tick,'ScanCount':count})

		fig = plt.figure(u'Ventas Por Dia') 
		ax = fig.add_subplot(111)
		xx = range(len(count))
		ax.bar(xx,count, width=0.8, align='center')
		ax.set_xticks(xx)
		ax.set_xticklabels(week)

		return plt.show()





	##################Departamentos################################################
	###############################################################################
	def barra_sun_dep(self):
		Dep = self.data[['DepartmentDescription','Upc','Weekday']]
		Dep = Dep.groupby(['Weekday','DepartmentDescription']).count()
		sun = Dep.loc[Dep.index.get_level_values('Weekday') == 'Sunday']
		dep = list(sun.index.get_level_values('DepartmentDescription'))
		count = list(sun['Upc'])
		csv = pd.DataFrame({'DepartmentDescription':dep,'Count':count})
		csv.to_csv('dep_domingo')

		fig = plt.figure(u'Departamentos solicitados en Domingo') 
		ax = fig.add_subplot(111)
		xx = range(len(count))
		ax.bar(xx,count, width=0.8, align='center')
		ax.set_xticks(xx)
		x = range(len(dep))
		ax.set_xticklabels(x)

		return plt.show()

	
	def barra_mon_dep(self):
		Dep = self.data[['DepartmentDescription','Upc','Weekday']]
		Dep = Dep.groupby(['Weekday','DepartmentDescription']).count()
		mon = Dep.loc[Dep.index.get_level_values('Weekday') == 'Monday']
		dep = list(mon.index.get_level_values('DepartmentDescription'))
		count = list(mon['Upc'])
		csv = pd.DataFrame({'DepartmentDescription':dep,'Count':count})
		csv.to_csv('dep_lunes')

		fig = plt.figure(u'Departamentos solicitados en lunes') 
		ax = fig.add_subplot(111)
		xx = range(len(count))
		ax.bar(xx,count, width=0.8, align='center')
		ax.set_xticks(xx)
		x = range(len(dep))
		ax.set_xticklabels(x)

		return plt.show()

	
	def barra_tus_dep(self):	
		Dep = self.data[['DepartmentDescription','Upc','Weekday']]
		Dep = Dep.groupby(['Weekday','DepartmentDescription']).count()
		tus = Dep.loc[Dep.index.get_level_values('Weekday') == 'Tuesday']
		dep = list(tus.index.get_level_values('DepartmentDescription'))
		count = list(tus['Upc'])
		csv = pd.DataFrame({'DepartmentDescription':dep,'Count':count})
		csv.to_csv('dep_martes')


		fig = plt.figure(u'Departamentos solicitados en Martes') 
		ax = fig.add_subplot(111)
		xx = range(len(count))
		ax.bar(xx,count, width=0.8, align='center')
		ax.set_xticks(xx)
		x = range(len(dep))
		ax.set_xticklabels(x)

		return plt.show()

	def barra_wed_dep(self):
		Dep = self.data[['DepartmentDescription','Upc','Weekday']]
		Dep = Dep.groupby(['Weekday','DepartmentDescription']).count()
		wed = Dep.loc[Dep.index.get_level_values('Weekday') == 'Wednesday']
		dep = list(wed.index.get_level_values('DepartmentDescription'))
		count = list(wed['Upc'])
		csv = pd.DataFrame({'DepartmentDescription':dep,'Count':count})
		csv.to_csv('dep_miercoles')


		fig = plt.figure(u'Departamentos solicitados en Miercoles') 
		ax = fig.add_subplot(111)
		xx = range(len(count))
		ax.bar(xx,count, width=0.8, align='center')
		ax.set_xticks(xx)
		x = range(len(dep))
		ax.set_xticklabels(x)

		return plt.show()
	
	def barra_thu_dep(self):
		Dep = self.data[['DepartmentDescription','Upc','Weekday']]
		Dep = Dep.groupby(['Weekday','DepartmentDescription']).count()
		thu = Dep.loc[Dep.index.get_level_values('Weekday') == 'Thursday']
		dep = list(thu.index.get_level_values('DepartmentDescription'))
		count = list(thu['Upc'])
		csv = pd.DataFrame({'DepartmentDescription':dep,'Count':count})
		csv.to_csv('dep_jueves')

		fig = plt.figure(u'Departamentos solicitados en Jueves') 
		ax = fig.add_subplot(111)
		xx = range(len(count))
		ax.bar(xx,count, width=0.8, align='center')
		ax.set_xticks(xx)
		x = range(len(dep))
		ax.set_xticklabels(x)

		return plt.show()
	
	def barra_fri_dep(self):
		Dep = self.data[['DepartmentDescription','Upc','Weekday']]
		Dep = Dep.groupby(['Weekday','DepartmentDescription']).count()
		fri = Dep.loc[Dep.index.get_level_values('Weekday') == 'Friday']
		dep = list(fri.index.get_level_values('DepartmentDescription'))
		count = list(fri['Upc'])
		csv = pd.DataFrame({'DepartmentDescription':dep,'Count':count})
		csv.to_csv('dep_viernes')


		fig = plt.figure(u'Departamentos solicitados en Viernes') 
		ax = fig.add_subplot(111)
		xx = range(len(count))
		ax.bar(xx,count, width=0.8, align='center')
		ax.set_xticks(xx)
		x = range(len(dep))
		ax.set_xticklabels(x)

		return plt.show()
	
	def barra_sat_dep(self):
		Dep = self.data[['DepartmentDescription','Upc','Weekday']]
		Dep = Dep.groupby(['Weekday','DepartmentDescription']).count()
		sat = Dep.loc[Dep.index.get_level_values('Weekday') == 'Saturday']
		dep = list(sat.index.get_level_values('DepartmentDescription'))
		count = list(sat['Upc'])
		csv = pd.DataFrame({'DepartmentDescription':dep,'Count':count})
		csv.to_csv('dep_sabado')


		fig = plt.figure(u'Departamentos solicitados en Sabado') 
		ax = fig.add_subplot(111)
		xx = range(len(count))
		ax.bar(xx,count, width=0.8, align='center')
		ax.set_xticks(xx)
		x = range(len(dep))
		ax.set_xticklabels(x)

		return plt.show()
	################################################################################


	


