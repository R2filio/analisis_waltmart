import pandas as pd
import psycopg2 as ps


data = pd.read_csv('Dataset_Entrevista_Walmart.csv',encoding='utf-8-sig',engine='python',converters = {'Upc':lambda x:str.strip(x)})
#print(data.info())
conexion = ps.connect(host='localhost',
						database='waltmart',
						user='arturo',
						port=5432, 
						password='Cafa2492@')

cur = conexion.cursor()
#Agrupado por ticket
ticket = data.groupby(['VisitNumber','Upc']).mean()
#print(ticket.head(20))

#Agrupando por dia 
week = data.groupby(['Weekday','Upc']).mean()
#print(week.head(20))

#Ticket por dia 
#Agrupando por dia 
tick_day = data[['Weekday','VisitNumber','Upc']]
#tick_day = tick_day[tick_day['Weekday'] == 'Sunday']
#print(tick_day.head())
tick_day = tick_day.groupby(['Weekday','VisitNumber']).count()
#print(tick_day)
######################Estadistica de tickets por dia########################
############################################################################ 
sun = tick_day.loc[tick_day.index.get_level_values('Weekday') == 'Sunday']
e_sun = sun.describe()
mon = tick_day.loc[tick_day.index.get_level_values('Weekday') == 'Monday']
e_mon = mon.describe()
tue = tick_day.loc[tick_day.index.get_level_values('Weekday') == 'Tuesday']
e_tue = tue.describe()
wed = tick_day.loc[tick_day.index.get_level_values('Weekday') == 'Wednesday']
e_wed = wed.describe()
thu = tick_day.loc[tick_day.index.get_level_values('Weekday') == 'Thursday']
e_thu = thu.describe()
fri = tick_day.loc[tick_day.index.get_level_values('Weekday') == 'Friday']
e_fri = fri.describe()
sat = tick_day.loc[tick_day.index.get_level_values('Weekday') == 'Saturday']
e_sat = sat.describe()


est_weekday = pd.DataFrame({'weekday':['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
							'count':[e_sun.iloc[0][0],e_mon.iloc[0][0],e_tue.iloc[0][0],e_wed.iloc[0][0],e_thu.iloc[0][0],e_fri.iloc[0][0],e_sat.iloc[0][0]],
							'mean':[e_sun.iloc[1][0],e_mon.iloc[1][0],e_tue.iloc[1][0],e_wed.iloc[1][0],e_thu.iloc[1][0],e_fri.iloc[1][0],e_sat.iloc[1][0]],
							'std':[e_sun.iloc[2][0],e_mon.iloc[2][0],e_tue.iloc[2][0],e_wed.iloc[2][0],e_thu.iloc[2][0],e_fri.iloc[2][0],e_sat.iloc[2][0]],
							'min':[e_sun.iloc[3][0],e_mon.iloc[3][0],e_tue.iloc[3][0],e_wed.iloc[3][0],e_thu.iloc[3][0],e_fri.iloc[3][0],e_sat.iloc[3][0]],
							'q1':[e_sun.iloc[4][0],e_mon.iloc[4][0],e_tue.iloc[4][0],e_wed.iloc[4][0],e_thu.iloc[4][0],e_fri.iloc[4][0],e_sat.iloc[4][0]],
							'q2':[e_sun.iloc[5][0],e_mon.iloc[5][0],e_tue.iloc[5][0],e_wed.iloc[5][0],e_thu.iloc[5][0],e_fri.iloc[5][0],e_sat.iloc[5][0]],
							'q3':[e_sun.iloc[6][0],e_mon.iloc[6][0],e_tue.iloc[6][0],e_wed.iloc[6][0],e_thu.iloc[6][0],e_fri.iloc[6][0],e_sat.iloc[6][0]],
							'max':[e_sun.iloc[7][0],e_mon.iloc[7][0],e_tue.iloc[7][0],e_wed.iloc[7][0],e_thu.iloc[7][0],e_fri.iloc[7][0],e_sat.iloc[7][0]]})

est_weekday = est_weekday.sort_values('count',ascending=False)

for i, row in est_weekday.iterrows():
	sqlquery = "INSERT INTO stadistic_day_ticket (weekday,count,mean,std,min,first_quartil,second_quartil,third_quartil,max) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s);"
	parametros = (row['weekday'],
					row['count'],
					row['mean'],
					row['std'],
					row['min'],
					row['q1'],
					row['q2'],
					row['q3'],
					row['max'])
	cur.execute(sqlquery,parametros)
	conexion.commit()


##############################################################################

####################### Estadistica de tickets Semanales ################################
ventas_d = data[['Weekday','ScanCount']]
ventas_d= ventas_d.groupby(['Weekday']).sum()
est_week = ventas_d['ScanCount'].describe()

est_week = pd.DataFrame({'count':[est_week.iloc[0]],
						'mean':[est_week.iloc[1]],
						'std':[est_week.iloc[2]],
						'min':[est_week.iloc[3]],
						'q1':[est_week.iloc[4]],
						'q2':[est_week.iloc[5]],
						'q3':[est_week.iloc[6]],
						'max':[est_week.iloc[7]]})

est_week = est_week.sort_values('count',ascending=False)
for i, row in est_week.iterrows():
	sqlquery = "INSERT INTO stadistic_week_ticket (count,mean,std,min,first_quartil,second_quartil,third_quartil,max) VALUES (%s,%s,%s,%s,%s,%s,%s,%s);"
	parametros = (row['count'],
				row['mean'],
				row['std'],
				row['min'],
				row['q1'],
				row['q2'],
				row['q3'],
				row['max'])

	cur.execute(sqlquery,parametros)
	conexion.commit()

##############################################################################


############################Productos###########################################
################################################################################
Productos = data[['Upc','Weekday','ScanCount','DepartmentDescription']]
Productos = Productos.groupby(['Weekday','Upc','DepartmentDescription']).sum()


sun = Productos.loc[Productos.index.get_level_values('Weekday') == 'Sunday']
id_p = list(sun.index.get_level_values('Upc'))
dep_d = list(sun.index.get_level_values('DepartmentDescription'))
count = list(sun['ScanCount'])
dic = {}
dic = {'Upc':id_p,'DepartmentDescription':dep_d,'ScanCount':count}
sun = pd.DataFrame(dic)
sun = sun.sort_values('ScanCount',ascending=False)
for i, row in sun.iterrows():
	sqlquery = "INSERT INTO product_sun (Upc,DepartmentDescription,ScanCount) VALUES (%s,%s,%s)"
	parametros = (row['Upc'],
				row['DepartmentDescription'],
				row['ScanCount'])
	cur.execute(sqlquery,parametros)
	conexion.commit()
#########################################################################################

mon = Productos.loc[Productos.index.get_level_values('Weekday') == 'Monday']
id_p = list(mon.index.get_level_values('Upc'))
dep_d = list(mon.index.get_level_values('DepartmentDescription'))
count = list(mon['ScanCount'])
dic = {}
dic = {'Upc':id_p,'DepartmentDescription':dep_d,'ScanCount':count}
mon = pd.DataFrame(dic)
mon = mon.sort_values('ScanCount',ascending=False)
for i, row in mon.iterrows():
	sqlquery = "INSERT INTO product_mon (Upc,DepartmentDescription,ScanCount) VALUES (%s,%s,%s)"
	parametros = (row['Upc'],
				row['DepartmentDescription'],
				row['ScanCount'])
	cur.execute(sqlquery,parametros)
	conexion.commit()
################################################################################################

tus = Productos.loc[Productos.index.get_level_values('Weekday') == 'Tuesday']
id_p = list(tus.index.get_level_values('Upc'))
dep_d = list(tus.index.get_level_values('DepartmentDescription'))
count = list(tus['ScanCount'])
dic = {}
dic = {'Upc':id_p,'DepartmentDescription':dep_d,'ScanCount':count}
tus = pd.DataFrame(dic)
tus = tus.sort_values('ScanCount',ascending=False)
for i, row in tus.iterrows():
	sqlquery = "INSERT INTO product_tus (Upc,DepartmentDescription,ScanCount) VALUES (%s,%s,%s)"
	parametros = (row['Upc'],
				row['DepartmentDescription'],
				row['ScanCount'])
	cur.execute(sqlquery,parametros)
	conexion.commit()
###################################################################################################

wed = Productos.loc[Productos.index.get_level_values('Weekday') == 'Wednesday']
id_p = list(wed.index.get_level_values('Upc'))
dep_d = list(wed.index.get_level_values('DepartmentDescription'))
count = list(wed['ScanCount'])
dic = {}
dic = {'Upc':id_p,'DepartmentDescription':dep_d,'ScanCount':count}
wed = pd.DataFrame(dic)
wed = wed.sort_values('ScanCount',ascending=False)
for i, row in wed.iterrows():
	sqlquery = "INSERT INTO product_wed (Upc,DepartmentDescription,ScanCount) VALUES (%s,%s,%s)"
	parametros = (row['Upc'],
				row['DepartmentDescription'],
				row['ScanCount'])
	cur.execute(sqlquery,parametros)
	conexion.commit()
###################################################################################################

thu = Productos.loc[Productos.index.get_level_values('Weekday') == 'Thursday']
id_p = list(thu.index.get_level_values('Upc'))
dep_d = list(thu.index.get_level_values('DepartmentDescription'))
count = list(thu['ScanCount'])
dic = {}
dic = {'Upc':id_p,'DepartmentDescription':dep_d,'ScanCount':count}
thu = pd.DataFrame(dic)
thu = thu.sort_values('ScanCount',ascending=False)
for i, row in thu.iterrows():
	sqlquery = "INSERT INTO product_thu (Upc,DepartmentDescription,ScanCount) VALUES (%s,%s,%s)"
	parametros = (row['Upc'],
				row['DepartmentDescription'],
				row['ScanCount'])
	cur.execute(sqlquery,parametros)
	conexion.commit()
#####################################################################################################

fri = Productos.loc[Productos.index.get_level_values('Weekday') == 'Friday']
id_p = list(fri.index.get_level_values('Upc'))
dep_d = list(fri.index.get_level_values('DepartmentDescription'))
count = list(fri['ScanCount'])
dic = {}
dic = {'Upc':id_p,'DepartmentDescription':dep_d,'ScanCount':count}
fri = pd.DataFrame(dic)
fri = fri.sort_values('ScanCount',ascending=False)
for i, row in fri.iterrows():
	sqlquery = "INSERT INTO product_fri (Upc,DepartmentDescription,ScanCount) VALUES (%s,%s,%s)"
	parametros = (row['Upc'],
				row['DepartmentDescription'],
				row['ScanCount'])
	cur.execute(sqlquery,parametros)
	conexion.commit()
#########################################################################################################

sat = Productos.loc[Productos.index.get_level_values('Weekday') == 'Saturday']
id_p = list(sat.index.get_level_values('Upc'))
dep_d = list(sat.index.get_level_values('DepartmentDescription'))
count = list(sat['ScanCount'])
dic = {}
dic = {'Upc':id_p,'DepartmentDescription':dep_d,'ScanCount':count}
sat = pd.DataFrame(dic)
sat = sat.sort_values('ScanCount',ascending=False)
for i, row in sat.iterrows():
	sqlquery = "INSERT INTO product_sat (Upc,DepartmentDescription,ScanCount) VALUES (%s,%s,%s)"
	parametros = (row['Upc'],
				row['DepartmentDescription'],
				row['ScanCount'])
	cur.execute(sqlquery,parametros)
	conexion.commit()
#############################################################################################################


#Numero de ventas por ticket
ventas_t = data[['VisitNumber','ScanCount']]
ventas_t= ventas_t.groupby(['VisitNumber']).sum()

#numero de ventas por dia 
ventas_d = data[['Weekday','ScanCount']]
ventas_d= ventas_d.groupby(['Weekday']).sum()
#print(ventas_d)


##################Departamentos################################################
###############################################################################
Dep = data[['DepartmentDescription','Upc','Weekday']]
Dep = Dep.groupby(['Weekday','DepartmentDescription']).count()


sun = Dep.loc[Dep.index.get_level_values('Weekday') == 'Sunday']
mon = Dep.loc[Dep.index.get_level_values('Weekday') == 'Monday']
tus = Dep.loc[Dep.index.get_level_values('Weekday') == 'Tusday']
wed = Dep.loc[Dep.index.get_level_values('Weekday') == 'Wednesday']
thu = Dep.loc[Dep.index.get_level_values('Weekday') == 'Thursday']
fri = Dep.loc[Dep.index.get_level_values('Weekday') == 'Friday']
sat = Dep.loc[Dep.index.get_level_values('Weekday') == 'Saturday']
################################################################################






















